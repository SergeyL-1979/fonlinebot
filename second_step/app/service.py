from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, \
                          InlineKeyboardMarkup, InlineKeyboardButton
from emoji import emojize
from config import BOT_LEAGUES, BOT_LEAGUE_FLAGS, MINUTE
from database import cache, database as db
from app.dialogs import msg


MAIN_KB = ReplyKeyboardMarkup(
    resize_keyboard=True,
    one_time_keyboard=True
).row(
    KeyboardButton(msg.btn_online),
    KeyboardButton(msg.btn_config)
)

CONFIG_KB = InlineKeyboardMarkup().row(
    InlineKeyboardButton(msg.btn_back, callback_data='main_window'),
    InlineKeyboardButton(msg.config_btn_edit, callback_data='edit_config#')
).add(InlineKeyboardButton(msg.config_btn_delete, callback_data='delete_config'))


def leagues_kb(active_leagues: list, offset: int = 0):
    kb = InlineKeyboardMarkup()
    league_keys = list(BOT_LEAGUES.keys())[0+offset:5+offset]
    for lg_id in league_keys:
        if lg_id in active_leagues:
            kb.add(InlineKeyboardButton(
                f"{emojize(':white_heavy_check_mark:')} {BOT_LEAGUES[lg_id]}",
                callback_data=f'del_league_#{offset}#{lg_id}'
            ))
        else:
            kb.add(InlineKeyboardButton(
                BOT_LEAGUES[lg_id],
                callback_data=f'add_league_#{offset}#{lg_id}'
            ))
    kb.row(
        InlineKeyboardButton(
            msg.btn_back if offset else msg.btn_go,
            callback_data='edit_config#0' if offset else 'edit_config#5'),
        InlineKeyboardButton(msg.btn_save, callback_data='save_config')
    )
    return kb


def results_kb(leagues: list):
    params = [f'#{lg}' for lg in leagues]
    kb = InlineKeyboardMarkup()
    kb.add(InlineKeyboardButton(
        msg.update_results,
        callback_data=f"update_results{''.join(params)}"
    ))
    return kb


async def get_league_ids(user_id: str) -> list:
    """Функция получает id лиг пользователя в базе данных"""
    leagues = cache.lrange(f'u{user_id}', 0, -1)
    if leagues is None:
        leagues = await db.select_users(user_id)
        if leagues is not None:
            leagues = leagues.split(",")
            [cache.lpush(f'u{user_id}', lg_id) for lg_id in leagues]
        else:
            return []
    return leagues


async def get_league_names(ids: list) -> str:
    """Функция собирает сообщение с названиями лиг из id"""
    leagues_text = ""
    for i, lg_id in enumerate(ids, start=1):
        if i != 1:
            leagues_text += "\n"
        leagues_text += msg.league_row.format(
            i=i,
            flag=emojize(BOT_LEAGUE_FLAGS.get(lg_id, "-")),
            name=BOT_LEAGUES.get(lg_id, "-")
        )
    return leagues_text


def update_leagues(user_id: str, data: str):
    """Функция добаляет или удаляет id лиги для юзера"""
    league_id = data.split("#")[-1]  # data ~ add_league_#5#345
    if data.startswith("add"):
        cache.lpush(f'u{user_id}', league_id)
    else:
        cache.lrem(f'u{user_id}', 0, league_id)


async def generate_results_answer(ids: list) -> str:
    """Функция создaет сообщение для вывода результатов матчей"""
    results = await get_last_results(ids)
    if results:
        text_results = results_to_text(results)
        return msg.results.format(matches=text_results)
    else:
        return msg.no_results


def ids_to_key(ids: list) -> str:
    """Стандартизация ключей для хранения матчей"""
    ids.sort()
    return ','.join(ids)


async def parse_matches(ids: list) -> list:
    """Функция получения матчей по API"""
    # логику напишем в следующей части
    return []


async def get_last_results(league_ids: list) -> list:
    lg_key = ids_to_key(league_ids)
    last_results = cache.jget(lg_key)
    if last_results is None:
        last_results = await parse_matches(league_ids)
        if last_results:
            # добавляем новые матчи, если они есть
            cache.jset(lg_key, last_results, MINUTE)
    return last_results


def results_to_text(matches: list) -> str:
    """
    Функция генерации сообщения с матчами
    """
    # логику напишем в следующей части
    ...
